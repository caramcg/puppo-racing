import turtle
import random
import sys
import os

# Constant values
WIDTH, HEIGHT = 500, 500
ICONS = ['dogPixels/dog1.gif',
         'dogPixels/dog2.gif',
         'dogPixels/dog3.gif',
         'dogPixels/dog4.gif',
         'dogPixels/dog5.gif',
         'dogPixels/dog6.gif',
         'dogPixels/dog7.gif',
         'dogPixels/dog8.gif',
         'dogPixels/dog9.gif',
         'dogPixels/dog10.gif']


def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)


def get_num_of_racers(screen):
    racers = 0
    print("Getting number of racers...")
    racers = screen.numinput('racer', 'Enter the number of racers (2 - 10): ', minval=2, maxval=10)
    #write_on_screen(racers)
    racers = int(racers)
    return racers


# def write_on_screen(racers):
#     if racers == 0:
#         write_num = 1
#         spacingx = WIDTH // 11
#
#         pen = turtle.Turtle()
#         pen.hideturtle()
#         pen.penup()
#         pen.setpos(-200, 0)
#         pen.speed(1)
#         pen.write("How many racers?", font=('Calibri', 40, 'bold'))
#
#         while write_num < 10:
#             write_num = write_num + 1
#             write = turtle.Turtle()
#             write.hideturtle()
#             write.speed(0)
#             write.penup()
#
#             write.setpos(-WIDTH // 2 + (write_num -1) * spacingx, -HEIGHT / 2 + 50)
#             write.write(write_num, font=('Calibri', 30, 'bold'))


def name_dogs(result):
    name = []

    #for i in result:
    #print(i)
    if "dogPixels/dog1.gif" in result:
        name.append("Lucy")
        #result.remove(i)
        #print(name)
    if "dogPixels/dog2.gif" in result:
        name.append("Bowser")
        #print(name)
        #name = "Bowser"
    if "dogPixels/dog3.gif" in result:
        name.append("Teddy")
        #name = "Teddy"
    if "dogPixels/dog4.gif" in result:
        name.append("Inu")
        #name = "Inu"
    if "dogPixels/dog5.gif" in result:
        name.append("Boomer")
        #name = "Boomer"
    if "dogPixels/dog6.gif" in result:
        name.append("Keira")
        #name = "Keira"
    if "dogPixels/dog7.gif" in result:
        name.append("Pugsly")
        #name = "Pugsly"
    if "dogPixels/dog8.gif" in result:
        name.append("TicTac")
        #name = "TicTac"
    if "dogPixels/dog9.gif" in result:
        name.append("Luna")
        #name = "Luna"
    if "dogPixels/dog10.gif" in result:
        name.append("Chowder")
        #name = "Chowder"

    print(name)
    return name


def race(icons):
    dogs = create_dogs(icons)
    print("To the races!")
    while True:
        for racer in dogs:
            distance = random.randrange(1, 20)
            racer.forward(distance)

            x, y = racer.pos()
            if y >= HEIGHT // 2 - 10:
                result = icons[dogs.index(racer)]
                dog_name = name_dogs(result)
            #DOG WRITES WHO WINS
                racer.setpos(-150, 0)
                racer.write(''.join(dog_name) + ' WINS!', font=('Comic Sans', 30, 'bold'))
                racer.setpos(100, -50)
                racer.write('BORK! BORK!', font=('Comic Sans', 10, 'bold'))

                return dog_name


def create_dogs(icons):
    dogs = []
    spacingx = WIDTH // (len(icons) + 1)

    # enumerate gives index AND value
    for i, icon in enumerate(icons):
        icon = resource_path(icon)
        racer = turtle.Turtle()
        turtle.register_shape(icon)
        racer.shape(icon)
        racer.left(90)
        racer.penup()
        # set position to bottom of screen
        racer.setpos(-WIDTH // 2 + (i + 1) * spacingx, -HEIGHT / 2 + 50)
        dogs.append(racer)

    return dogs


def init_screen(screen):
    # initialize screen
    screen.setup(WIDTH, HEIGHT)
    screen.title('Dog Racing!')
    screen.bgcolor('light blue')


def play_or_exit(screen):
    again = screen.textinput('Play again?', 'Would you like to play again? yes or no')

    if again == 'yes':
        screen.clear()
        main()
    else:
        screen.bye()
        # try:
        #     exit()
        # except IOError:
        #     print('exit() did not run')


def get_bet(screen, icons, wallet):
    dogs = name_dogs(icons)
    dog_bet = screen.textinput('Who will win?', 'Type the name of the dog you think will win\n' + ' '.join(dogs))

    money_bet = screen.numinput('Place your bet!', 'Minimum is $5 \nHow much will you bet?\nYou have $' + str(wallet), minval=5, maxval=wallet)
    money_bet = int(money_bet)

    newwallet = wallet - money_bet
    wallet = newwallet

    print("Getting bet...")

    return [wallet, money_bet, dog_bet]


def check_bet(bet, winner, racers, wallet):
    newwallet = bet[0]
    money_bet = bet[1]
    dog_bet = bet[2]

    #print(''.join(winner))
    #print(dog_bet)

    if ''.join(winner) == str(dog_bet):
        print('yay you won')
        if racers <= 5:
            newwallet = wallet + (money_bet * 2)
        elif racers > 5:
            newwallet = wallet + (money_bet * 4)
        else:
            print('something has gone wrong with the bet calc')
    else:
        print('We did not win')

    print(newwallet)
    return newwallet


def save_winnings(newwallet):
    file_name = resource_path('saveWallet.txt')
    f = open(file_name, "w")
    f.write(str(newwallet))
    print("Wallet has been updated to: " + str(newwallet))
    f.close()

    return


def load_wallet():
    try:
        file_name = resource_path('saveWallet.txt')
        with open(file_name, "r") as file:
            wallet = (file.read())
            #print(wallet)
    except IOError:
        #print("File does not exist")
        wallet = 50

    return int(wallet)


def main():
    #Set up the window
    screen = turtle.Screen()
    init_screen(screen)

    #Load Wallet
    wallet = load_wallet()
    #print('Loaded:', wallet)

    #Set up the dogs
    racers = get_num_of_racers(screen)
    random.shuffle(ICONS)  # shuffle up the icons into random order
    icons = ICONS[:racers]  # slice operator to select dog icons based on input

    #Set up Bet
    bet = get_bet(screen, icons, wallet)

    #OFF TO THE RACES!
    winner = race(icons)

    #Check Bet
    newwallet = check_bet(bet, winner, racers, wallet)

    #Save winnings
    #print(newwallet)
    save_winnings(newwallet)


    #Continue?
    play_or_exit(screen)


main()



